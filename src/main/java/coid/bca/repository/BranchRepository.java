package coid.bca.repository;

import org.springframework.stereotype.Repository;

@Repository
//public class BranchRepository{
public class BranchRepository implements MyRepository {
	public void save() {
		System.out.println("Save Branch");
	}
	public void edit() {
		System.out.println("Edit Branch");
	}
	public void delete() {
		System.out.println("Delete Branch");
	}
}

