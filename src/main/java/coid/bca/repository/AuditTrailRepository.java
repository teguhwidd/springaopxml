package coid.bca.repository;

import org.springframework.stereotype.Repository;

@Repository
public class AuditTrailRepository {
	public void save(String action) {
		System.out.println("Save Audit Trail. Action: "+ action);
	}
	public void delete(String action) {
		System.out.println("Delete Audit Taril. Action: "+ action);
	}
	public void edit(String action) {
		System.out.println("Edit Audit Taril. Action: "+ action);
	}
}
