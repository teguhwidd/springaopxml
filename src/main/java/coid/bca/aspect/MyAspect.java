package coid.bca.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MyAspect {
	
	
	@Around("execution(* coid.bca.repository.*.save(..))")
	public void around() {
		System.out.println("MyAspect Around. ");
	}
	@Before("execution(* coid.bca.repository.*.save(..))")
	public void before() {
		System.out.println("MyAspect before. ");
	}
	@After("execution(* coid.bca.repository.*.save(..))")
	public void after() {
		System.out.println("MyAspect after. ");
	}
	@AfterReturning("execution(* coid.bca.repository.*.save(..))")
	public void afterReturning() {
		System.out.println("MyAspect after returning. ");
	}
}
