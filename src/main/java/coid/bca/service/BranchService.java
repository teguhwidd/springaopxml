package coid.bca.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coid.bca.repository.AuditTrailRepository;
import coid.bca.repository.BranchRepository;
import coid.bca.repository.MyRepository;

//@Service("cabangService") pemberian nama saat dipanggil context.getBean
@Service
public class BranchService {
	@Autowired
	private AuditTrailRepository auditTrailRepository;
	@Autowired
	//private BranchRepository branchRepository;
	private MyRepository branchRepository;
	
	public void save(){
		auditTrailRepository.save("Save Branch");
		branchRepository.save();
	}
	public void delete() {
		auditTrailRepository.delete("Delete Branch");
	}
	public void edit() {
		auditTrailRepository.edit("Edit Branch");
	}
	
	public void setAuditTrailRepository(AuditTrailRepository auditTrailRepository) {
		this.auditTrailRepository = auditTrailRepository;
	}
	public void setBranchRepository(BranchRepository branchRepository) {
		this.branchRepository = branchRepository;
	}
	
}
