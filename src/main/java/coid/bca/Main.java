package coid.bca;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import coid.bca.service.BranchService;

public class Main {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		BranchService branchService = (BranchService) context.getBean("branchService");
		branchService.save();
		branchService.edit();
		branchService.delete();
	}
}
